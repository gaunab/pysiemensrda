#!env python3
# -*- coding: utf-8 -*-

from numpy.fft import fft, fftshift, ifft, ifftshift
def subtraction(cplx1, cplx2):
    """ 
    Perform Subtraction in Frequency-Domain. Input is complex vector
    """

    spec1 = fftshift(fft(cplx1))
    spec2 = fftshift(fft(cplx2))

    diff = spec1-spec2

    cplxdiff = ifft(ifftshift(diff))

    return cplxdiff
