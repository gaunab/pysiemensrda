#!env python3
# -*- coding: utf-8 -*-

import numpy as np
from pathlib import Path



def read_header(raw_header):
    """
    Turn header from RDA-File (bytes) into dictionary
    """

    try:
        header_lines = raw_header.decode().splitlines()
    except UnicodeDecodeError:
        header_lines = raw_header.decode('latin-1',errors='replace').splitlines()
    header = {}
    for headline in header_lines:
        res = headline.split(":")
        prop = res[0]
        val = ':'.join(res[1:])
        header[prop] = val
    
    return header


def read_rda(path_str):
    """
    Read a Siemens RDA-File
    
    Parameters
    ---------
    path_str: str
        Complete Path and Filename to the rda-File

    Returns
    -------

    Vector, Header
    """

    filep = Path(path_str);
    
    if not filep.is_file():
        raise IOError("%s is not a regular file" %path_str)

    with open(path_str,"rb") as f:
        data = f.read()

    # Now find end of header
    header_begin = data.find(b">>> Begin of header <<<")
    header_end = data.find(b">>> End of header <<<")
    # Read Header in seperate lines
    raw_header = data[header_begin+25:header_end] 
    raw_data = data[header_end+23:]
    header = read_header(raw_header) 
    vector_size = int(header['VectorSize'])

    # Read into ndarray:
    vector = np.ndarray(shape=(vector_size,), dtype='complex', buffer=raw_data)

    return(vector, raw_header)

def write_rda(filename, header, vector):
    """
    Write data into file

    Parameters
    ----------
    filename: str
        Filename where to write data to
    header: bytes
        Raw_HEADER (without ">>> Begin ...<<< " and ">>> End .. <<<"
    vector: ndarray of complex
        Data to write into file
    """

    data = b">>> Begin of header <<<\r\n" \
        + header \
        + b">>> End of header <<<\r\n" \
        + vector.tobytes()
    with open(filename,"wb") as f:
        f.write(data)

