#!env python3
# *-* coding: utf-8 *-*


from file_handler import read_rda, write_rda
from math_operations import subtraction

def calculate_difference(in1, in2, out):
    """
    Calculate de difference of two rda-files (in spec)

    out = in1 - in2

    Parameters
    ----------

    in1: str
        filename + path for first file
    in2: str
        filename + path for second  file
    out: str
        filename + path where to store file
    """

    vector1, header1 = read_rda(in1)
    vector2, header2 = read_rda(in2)

    diff_vector = subtraction(vector1, vector2)

    write_rda(out, header1, diff_vector)


def main():

    import argparse
    import sys

    parser = argparse.ArgumentParser()
    parser.add_argument('input1', type=str)
    parser.add_argument('input2', type=str)
    parser.add_argument('output', type=str)

    args = parser.parse_args()
    calculate_difference(args.input1, args.input2, args.output)

if __name__ == "__main__":
    main()

