#!env python3
# -*- coding: utf-8 -*-

import sys
from rda_diff import calculate_difference
from PyQt5.QtWidgets import QMainWindow,QApplication, QWidget,QAction, qApp, QPushButton, QHBoxLayout, QVBoxLayout, QFileDialog, QLineEdit, \
        QGroupBox,QLabel
from PyQt5.QtGui import QIcon
import PyQt5.QtCore as QtCore
import PyQt5.sip


class Rda_gui(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        mainWidget = QWidget()

        exitAct = QAction(QIcon('exit.png'), '&Exit', self)
        exitAct.setShortcut('Ctrl+Q')
        exitAct.setStatusTip('Exit application')
        exitAct.triggered.connect(qApp.quit)

        menubar = self.menuBar()
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(exitAct)
        
        self.statusBar()

        self.setWindowTitle('RDA Diff')
        self.show()

        btn_save = QPushButton('Save', self)
        btn_save.setToolTip('Save Results');
        btn_save.clicked.connect(self.save_result)
    
        vbox = QVBoxLayout()
        # vbox.addWidget(QFileDialog())
        files_box = QHBoxLayout()

        self.fileselect1 = FileSelector()
        self.fileselect2 = FileSelector()
        files_box.addWidget(self.fileselect1)
        files_box.addWidget(QLabel("<h1>&nbsp;- </h1>"))
        files_box.addWidget(self.fileselect2)
        
        vbox.addLayout(files_box)
        vbox.addWidget(btn_save)

        mainWidget.setLayout(vbox)
        self.setCentralWidget(mainWidget)

        self.setAcceptDrops(True)
        self.show()

    def save_result(self):
        f1 = self.fileselect1.filename
        f2 = self.fileselect2.filename
       
        out_name, filt = QFileDialog.getSaveFileName(self,("Save File")) 
                # ,filter=("Siemens RDA *.rda"))
        calculate_difference(f1,f2,out_name)

class FileSelectorLineEdit(QLineEdit):
    def __init__(self):
        super().__init__()
        self.setAcceptDrops(True)
        self.setDragEnabled(True)

    def dropEvent(self, event):
        if event.mimeData().hasUrls():
            event.accept()
            for url in event.mimeData().urls():
                path = url.toLocalFile()
            print(path)
        else:
            event.ignore()

        self.setText(path)

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls():
            event.accept()
        else:
            event.ignore()

class FileSelector(QGroupBox):

    def __init__(self):
        super().__init__()
    
        self.setAcceptDrops(True)
        vbox = QVBoxLayout()
        self.setLayout(vbox)
        self.edt_filename = FileSelectorLineEdit()
        self.edt_filename.setAcceptDrops(True)
        vbox.addWidget(self.edt_filename)

        btn_select = QPushButton("Select File")
        btn_select.clicked.connect(self.selectFile)
        vbox.addWidget(btn_select)

    def selectFile(self):
        fname, filt = QFileDialog.getOpenFileName(self, "Open File")
        print(fname)
        self.edt_filename.setText(fname)

    @property 
    def filename(self):
        return self.edt_filename.text()

    def dropEvent(self, event):
        if event.mimeData().hasUrls():
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
            for url in event.mimeData().urls():
                path = url.toLocalFile()
            print(path)
        else:
            event.ignore()

        self.edt_filename.setText(path)

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls():
            event.accept()
        else:
            event.ignore()

if __name__ == '__main__':
    
    app = QApplication(sys.argv)
    w = Rda_gui()
    
    sys.exit(app.exec_())

