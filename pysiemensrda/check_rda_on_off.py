#! env python3

"""
    Check the Water-Supression-State of a file by reading

"""


try:
    from .file_handler import read_rda, read_header
except ImportError:
    from file_handler import read_rda, read_header

from os import rename
from os.path import basename, abspath, join, dirname
from glob import  glob
import fnmatch



def determine_header_state(path):
    """
    Determine wheter the file is rda_on or rda_off by Header

    Parameters
    ----------
    path: str
        Path and Filename of the rda-file

    Returns
    -------
        rda_state: str
            'off', 'on', 'unknown'

    """

    # Read header from RDA-File
    _, raw_header = read_rda(path)
    # read values from Raw-Header
    header = read_header(raw_header)

    # The Instance can be used to check the state.
    try:
        instance_number = int(header['InstanceNumber'])
    except:
        instance_number = 0

    state = ''
    if instance_number == 1:
        state = 'off'
    elif instance_number == 2:
        state = 'on'
    else:
        state = 'unknown'

    return state

def determine_filename_state(path):
    """
    Determine wheter the file is rda_on or rda_off by Filename

    Parameters
    ----------
    path: str
        Path and Filename of the rda_file

    Returns
    -------
        rda_state: str
            'off', 'on', 'unknown'
    """

    # First extract filename and make it lowercase
    fname = basename(path).lower()

    # create empty return string
    state = ''

    if 'off' in fname:
        state = 'off'
    elif 'on' in fname:
        state = 'on'
    else:
        state = 'unknown'

    return state

def compare_states(path):
    """ Compae the Filename-State and Header-State for all RDA-Files
    residing in a Directory """


    dirname = abspath(path)

    suspicios_files = []

    for fname in glob(f'{dirname}/**/*.rda', recursive=True):
        fnamestate = determine_filename_state(fname)
        headerstate = determine_header_state(fname)

        if fnamestate != 'unknown':
            if fnamestate != headerstate:
                suspicios_files.append(fname)

    return suspicios_files

def correct_filenames(paths):
    """ Correct the Filenames according to  Header-States

    Parameters
    ----------
    paths: list of str
        All Filenames that shall be renamed

    """

    # First rename_them with a temporal filename
    for fname in paths:
        try:
            rename(fname, f'{fname}_tmp')
        except OSError:
            print(f'Could not rename {fname}')

    for fname in paths:
        headerstate = determine_header_state(f'{fname}_tmp')
        fnamestate = determine_filename_state(f'{fname}_tmp')

        new_name = join(dirname(fname), basename(fname).replace(fnamestate, headerstate))
        print(f'renaming: {fname} to {new_name}')
        try:
            rename(f'{fname}_tmp', new_name)
        except OSError:
            print(f'Could not rename: {fname} to {new_name}')




def main():
    import argparse

    parser = argparse.ArgumentParser(description='Find RDA-Files where Filename and Headerstate are not matching')
    parser.add_argument('path', type=str)
    parser.add_argument('-c', '--correct', action='store_true', help='Automatically correct the filenames')
    args = parser.parse_args()

    suspicios_files = compare_states(args.path)

    if not args.correct:
        for fname in suspicios_files:
            print(fname)
    else:
        correct_filenames(suspicios_files)


if __name__ == '__main__':
    main()

