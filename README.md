# pysiemensrda

This python tool is meant to read and write RDA files from Siemens MR-Spectroscopy. 

### Intention
Our intention for the implementation was to create a tool which is able to calculate the diff of `ON - OFF`

## Usage

    python rda_diff.py [INFILE1] [INFILE2] [OUTFILE]
    
or use the GUI

    rda_diff_gui
    
where you can either select the files via File-Dialog or DragAndDrop them into the gui.
You can find a precompiled executable (created with pyinstaller) in the [Downloads](https://bitbucket.org/gaunab/pysiemensrda/downloads/)
section.